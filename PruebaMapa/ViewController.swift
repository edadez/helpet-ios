//
//  ViewController.swift
//  PruebaMapa
//
//  Created by Roberto Dote on 16-09-14.
//  Copyright (c) 2014 Roberto Dote. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var theMapView: MKMapView!
    var anglobal = MKPetAnnotation()
    var locationManager: CLLocationManager!
    var coordenadas: CLLocationCoordinate2D!
    

    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    @IBAction func setMapType(sender: UISegmentedControl) {
        switch(sender.selectedSegmentIndex){
            case 0:
                self.theMapView.mapType = MKMapType.Standard
                break;
            case 1:
                self.theMapView.mapType = MKMapType.Satellite
                break;
            case 2:
                self.theMapView.mapType = MKMapType.Hybrid
                break;
            default:
                break;
        }
        
    }
    
    
    func mapView(mapView: MKMapView!, didUpdateUserLocation userLocation: MKUserLocation!){
        self.theMapView.setRegion( MKCoordinateRegionMake(userLocation.coordinate, MKCoordinateSpanMake(0.1, 0.1) ) , animated: true)
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!){
       //detiene la actualización de la localización
       self.locationManager.stopUpdatingLocation()
        
        var locationArray = locations as NSArray
        var locationObj = locationArray.lastObject as CLLocation
        self.coordenadas = locationObj.coordinate
        
        
        
//        var noLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(self.coordenadas.latitude, self.coordenadas.longitude)
//        var viewRegion:MKCoordinateRegion = MKCoordinateRegionMakeWithDistance(noLocation, 250, 250)
//        var adjustRegion:MKCoordinateRegion = theMapView.regionThatFits(viewRegion)
//        
//        if(isnan(adjustRegion.center.longitude))
//        {
//            println("error")
//        }else{
//            theMapView.setRegion(adjustRegion, animated: true)
//        }
//        
        println(self.coordenadas.latitude)
        println(self.coordenadas.longitude)
    }
    
    func initLocation(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logo = UIImage(named: "logo1")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        initLocation()
        
//      let urlPath: String = "http://helpet.cl/lib/jsonview.php?id=1"
        let urlPath: String = "http://helpet.cl/lib/json.php"
        var url: NSURL = NSURL(string: urlPath)
        var request1: NSURLRequest = NSURLRequest(URL: url)
        let queue:NSOperationQueue = NSOperationQueue()
        
        NSURLConnection.sendAsynchronousRequest(request1, queue: queue, completionHandler:{ (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            /* Your code */
            
            var err: NSError
            
            var jsonResult: NSArray = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSArray
//            println(jsonResult)
            for obj in jsonResult {
                
                var nombre: String = obj["nombre"] as String
                var distingue: String = obj["distingue"] as String
                var edad: String = obj["edad"] as String
                var especie: String = obj["especie"] as String
                var raza : String = obj["raza"] as String
                var rutaimg1 : String = obj["img1"] as String
                var rutaimg2 : String = obj["img2"] as String
                var rutaimg3 : String = obj["img3"] as String
                
                var lat: NSString = obj["lat"] as NSString
                var lng: NSString = obj["lng"] as NSString
                var lat1 = lat.doubleValue
                var lng1 = lng.doubleValue
                
                var pet = Pet()
                pet.nombre = nombre
                pet.edad = edad
                pet.especie = especie
                pet.raza = raza
                pet.distingue = distingue
                pet.rutaimg1 = rutaimg1
                pet.rutaimg2 = rutaimg2
                pet.rutaimg3 = rutaimg3
                
                self.cargarMarkers(lat1, lng: lng1, pet: pet)
                
            }
            
        })
        
    }
    
    func cargarMarkers(lat: Double, lng: Double, pet: Pet){
        
        var latitude:CLLocationDegrees  = lat
        var longitude:CLLocationDegrees = lng
        
        var churchLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        var theUlmMinsterAnnotation = MKPetAnnotation()
        
        theUlmMinsterAnnotation.coordinate = churchLocation
        theUlmMinsterAnnotation.title = pet.nombre
        theUlmMinsterAnnotation.subtitle = pet.distingue
        
        theUlmMinsterAnnotation.nombre = pet.nombre
        theUlmMinsterAnnotation.edad = pet.edad
        theUlmMinsterAnnotation.especie = pet.especie
        theUlmMinsterAnnotation.raza = pet.raza
        theUlmMinsterAnnotation.distingue = pet.distingue
        theUlmMinsterAnnotation.rutaimg1 = pet.rutaimg1
        theUlmMinsterAnnotation.rutaimg2 = pet.rutaimg2
        theUlmMinsterAnnotation.rutaimg3 = pet.rutaimg3
        
        self.theMapView.addAnnotation(theUlmMinsterAnnotation)
        
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKPetAnnotation!) -> MKAnnotationView!{

        if annotation.isKindOfClass(MKPetAnnotation) {
        
            //--- FUNCIÓN PARA DIMENSIONAR IMAGEN
            let image = UIImage(named: "marker")
            var newSize:CGSize = CGSize(width: 30,height: 30)
            let rect = CGRectMake(0,0, newSize.width, newSize.height)
            UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)

            image.drawInRect(rect)
            
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // ---- FIN FUNCIÓN ----- resized image is stored in constant newImage
            
            var anView:MKAnnotationView = MKAnnotationView()
            anView.annotation = annotation as MKPetAnnotation
            anView.image = newImage
            anView.canShowCallout = true
            anView.enabled = true
            let button = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as UIButton
            //button.addTarget(self, action: "miFuncion", forControlEvents: UIControlEvents.TouchUpInside)
            anView.rightCalloutAccessoryView = button
            return anView
        }
        
        return nil
            
    }
    
    func miFuncion(){} //función que podría funcionar con un UIBUTTON con la función addtarget al botón
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!){
        
//        let mensaje:UIAlertView = UIAlertView()
//        mensaje.message = "Lat:  \(view.annotation.coordinate.latitude) \n Lon: \(view.annotation.coordinate.longitude)"
//        mensaje.title = "Mis coordenadas"
//        mensaje.addButtonWithTitle("Ok")
//        mensaje.show()
        var mianotacion:MKPetAnnotation = view.annotation as MKPetAnnotation
        anglobal = mianotacion
        
        self.loader.startAnimating()
        self.loader.hidden = false
        self.performSegueWithIdentifier("showDetails", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if(segue.identifier=="showDetails"){
            var detailViewController:DetailViewController = segue.destinationViewController as DetailViewController
            detailViewController.an = anglobal
        }
    }
    
    override func viewDidDisappear(animated: Bool){
        self.loader.stopAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

