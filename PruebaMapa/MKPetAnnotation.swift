//
//  MKPetAnnotation.swift
//  PruebaMapa
//
//  Created by Roberto Dote on 17-09-14.
//  Copyright (c) 2014 Roberto Dote. All rights reserved.
//

import Foundation
import UIKit
import MapKit

public class MKPetAnnotation:MKPointAnnotation{
    var nombre:String!
    var edad:String!
    var especie:String!
    var raza:String!
    var distingue:String!
    var rutaimg1:String!
    var rutaimg2:String!
    var rutaimg3:String!
}