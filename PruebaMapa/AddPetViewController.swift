//
//  AddPetViewController.swift
//  PruebaMapa
//
//  Created by Roberto Dote on 25-09-14.
//  Copyright (c) 2014 Roberto Dote. All rights reserved.
//

import UIKit

class AddPetViewController: UIViewController {
    

    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let url = NSURL(string: "http://helpet.cl/pet/add")
        let request = NSURLRequest(URL: url)
        webView.loadRequest(request)
        
        webView.scrollView.bounces = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
