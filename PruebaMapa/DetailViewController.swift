//
//  DetailViewController.swift
//  PruebaMapa
//
//  Created by Roberto Dote on 16-09-14.
//  Copyright (c) 2014 Roberto Dote. All rights reserved.
//

import UIKit
import MapKit
import MessageUI



class DetailViewController:UIViewController, MKMapViewDelegate, MFMailComposeViewControllerDelegate {
    var an = MKPetAnnotation()
    var images = [String]()
    var mc = MFMailComposeViewController()
    
    @IBOutlet weak var imagenes: UIScrollView!
    
    @IBOutlet weak var txtNombre: UILabel!
    @IBOutlet weak var txtEdad: UILabel!
    @IBOutlet weak var txtEspecie: UILabel!
    @IBOutlet weak var txtRaza: UILabel!
    @IBOutlet weak var txtDistingue: UITextView!
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem?.title  = "Volver"
        let nophoto = UIImage(named: "nophoto")
        
        var rutasimgs = [an.rutaimg1, an.rutaimg2, an.rutaimg3]
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
        
            for var i = 0; i < rutasimgs.count ; i++
            {
                if (rutasimgs[i] == "http://helpet.cl/img/nophoto.jpg")
                { rutasimgs[i] = "/img/nophoto.jpg" }
                
                let url = NSURL(string:"http://helpet.cl\(rutasimgs[i])");
                var imageData =  UIImage(data: NSData(contentsOfURL: url))
                
                if( (i+1) == 1 ){
                    if (rutasimgs[i] == "http://helpet.cl/img/nophoto.jpg"){
                        self.img1.image = nophoto;
                    }else{
                        self.img1.image = imageData;
                    }
                }
                if( (i+1) == 2 ){
                    if (rutasimgs[i] == "http://helpet.cl/img/nophoto.jpg"){
                        self.img2.image = nophoto;
                    }else{
                        self.img2.image = imageData;
                    }
                }
                if( (i+1) == 3 ){
                    if (rutasimgs[i] == "http://helpet.cl/img/nophoto.jpg"){
                        self.img3.image = nophoto;
                    }else{
                        self.img3.image = imageData;
                    }
                }
            }
      })
        
      txtNombre.text = "\(an.nombre)"
      txtEdad.text = "\(an.edad)"
      txtEspecie.text = "\(an.especie)"
      txtRaza.text = "\(an.raza)"
      txtDistingue.text = "\(an.distingue)"

    }
    
    @IBAction func btnEmail(sender: AnyObject) {
        //Código para realizar la llamada desde un número de telefono
        UIApplication.sharedApplication().openURL(NSURL(string:"tel://0123456789"))
        
        
        //Código para enviar correo
        //        self.mc = MFMailComposeViewController()
        //        
        //        if MFMailComposeViewController.canSendMail() {
        //            var emailTitle = "Test Email"
        //            var messageBody = "<h1>Learning iOS Programming!</h1>"
        //            var toRecipents:NSArray = ["edades@gmail.com"] as NSArray
        //            println("Enviando correo...")
        //            mc.mailComposeDelegate = self;
        //            mc.setSubject(emailTitle)
        //            mc.setMessageBody(messageBody, isHTML: true)
        //            mc.setToRecipients(toRecipents)
        //            
        //            mc.modalPresentationStyle = UIModalPresentationStyle.FullScreen
        //            mc.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        //            
        //            // Present mail view controller on screen
        //            self.presentViewController(mc, animated: true, completion: nil)
        //        }else{
        //            println("Error al enviar correo")
        //        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!){
//            switch (result)
//            {
//            case MFMailComposeResultCancelled:
//                println("Mail cancelled");
//                break;
//            case MFMailComposeResultSaved:
//                println("Mail saved");
//                break;
//            case MFMailComposeResultSent:
//                println("Mail sent");
//                break;
//            case MFMailComposeResultFailed:
//                println("Mail sent failure: \(error.localizedDescription)");
//                break;
//            default:
//                break;
//            }
        self.dismissViewControllerAnimated(true, completion: nil)
    }

        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
